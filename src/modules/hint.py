import random

from discord.ext import commands


class Hint:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def hint(self, ctx):

        # Hints for the game
        hints = [
            "If you ever encounter `Talis Moon`, a wise choice would be to abandon the fight.",
            "Cheaters end up banished on the moon, they can play there but do not gain XP, Bits, nothing.",
            "Make sure you are properly equipped when adventuring into wilderness! Some areas have strong enemies.",
            "`Night Blue` can sometimes help you out, but only if the situation is in his interest or gain.",
            "`Lunar Eyes` is always bored, and bothered by `Night Blue`.",
            "You need to be a certain level to access an area.",
            "The level cap is `200`, *as of now!*"
        ]

        # Counts the hints to be used in a randint
        hints_count = len(hints) - 1

        await ctx.send(hints[random.randint(0, hints_count)])


def setup(bot):
    bot.add_cog(Hint(bot))

