import os

import discord
from discord.ext import commands
import json

class Info:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def info(self, ctx):
        with open('./pegapy.json') as f:
            pegapy = json.load(f)
            version = pegapy.get('version')
            codename = pegapy.get('codename')
            author = pegapy.get('author')

        # The embed
        embed = discord.Embed(description='Information')
        embed.add_field(name='**Version:**', value=version, inline=False)
        embed.add_field(name='**Codename:**', value=codename, inline=False)
        embed.add_field(name='**Author(s):**', value=author, inline=False)

        await ctx.send(embed=embed)


def setup(bot):
    bot.add_cog(Info(bot))
